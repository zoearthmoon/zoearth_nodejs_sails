//聯絡我們功能
module.exports = {

    //登入
    login: function (req, res)
    {
        var viewData = z2.setViewData(req,res);

        if (z2.isLogin(req))
        {
            viewData.title = "會員登出";
        }
        else
        {
            viewData.title = "會員登入";
        }

        //提示訊息
        if (typeof req.session.message != 'undefined' )
        {
            viewData.message = req.session.message;
            req.session.message = '';
        }

        //登入或登出
        if (z2.isLogin(req))
        {
            return res.view('users/logout',viewData);
        }
        else
        {
            return res.view('users/login',viewData);
        }
    },

    //登入
    loginAction: function (req, res)
    {
        var viewData = z2.setViewData(req,res);

        var post = req.allParams();
        db_users.checkLogin(post.username,post.password).then(function(result){
            if (result)
            {
                db_users.setLogin(req,post.username,post.password,function (result) {
                    req.session.message = "登入成功";
                    return res.redirect(sails.getUrlFor('usersController.login'));
                });
            }
            else
            {
                req.session.message = "登入失敗，帳號密碼錯誤";
                return res.redirect(sails.getUrlFor('usersController.login'));
            }
        });
    },

    //登出
    logoutAction: function (req, res)
    {
        var viewData = z2.setViewData(req,res);
        z2.setLogout(req);
        req.session.message = "登出成功";
        return res.redirect(sails.getUrlFor('usersController.login'));
    },

};
