//聯絡我們功能
module.exports = {
    //聯絡我們 socket 功能
    socket: function(req, res) {
        if (!req.isSocket)
        {
            return res.badRequest();
        }
        //開啟socket
        //contactRoom 連線room
        sails.sockets.join(req, 'contactRoom', function(err) {
            if (err)
            {
                return res.serverError(err);
            }
            return res.ok();
        });
    },

    //action名稱.類似zend 中的action
    list: function (req, res)
    {
        var viewData = z2.setViewData(req,res);
        viewData.title = "聯絡我們";

        //提示訊息
        if (typeof req.session.message != 'undefined' )
        {
            viewData.message = req.session.message;
            req.session.message = '';
        }

        //訊息列表
        db_contact.getUsers(function(users){
            viewData.userDatas = users;
            //設定view的值
            return res.view('contact/list',viewData);
        });
    },

    //action名稱.類似zend 中的action
    form: function (req, res)
    {
        var viewData = z2.setViewData(req,res);
        viewData.title = "聯絡我們表單";

        //是否為編輯
        var post = req.allParams();
        userData = {};

        //整理會用到資料.避免呼叫錯誤
        var goUserData = {};
        goUserData = db_contact.getAttrs(goUserData,userData,1);
        viewData.userData = goUserData;

        //設定view的值
        return res.view('contact/form',viewData);
    },

    //編輯資料
    edit: function (req, res)
    {
        var viewData = z2.setViewData(req,res);
        viewData.title = "聯絡我們表單-編輯資料";

        //是否為編輯
        var post = req.allParams();
        userData = {};
        //取得資料
        if (!(typeof post.guid != 'undefined' && post.guid > 0 ))
        {
            //提示訊息
            return res.serverError('ERROR 0063 找不到使用者!');
        }

        db_contact.getUser(post.guid,function(us){
            userData = us;

            //整理會用到資料.避免呼叫錯誤
            var goUserData = {};
            goUserData = db_contact.getAttrs(goUserData,userData,1);
            viewData.userData = goUserData;
            //設定view的值
            return res.view('contact/form',viewData);
        });
    },

    //寫入資料庫
    add: function (req, res)
    {
        //20160421 zoearth
        var post = req.allParams();
        var inputData = {
            'username': post.username,
            'password': post.password,
            'name':     post.name,
            'gender':   post.gender,
            'birthday': post.birthday.year+'-'+post.birthday.month+'-'+post.birthday.day,
            'tel':      post.tel,
            'address':  post.address,
            'note':     post.note,
        };

        if (typeof post.guid != 'undefined' )
        {
            inputData.guid = post.guid;
        }

        var userData = {};
        db_contact.creatAndSave(inputData,function(result){
            userData = result;
            //提示訊息
            req.session.message = "新增完成["+userData.name+"]";

            if (typeof post.guid != 'undefined' )
            {
                //提示訊息
                req.session.message = "修改完成["+userData.name+"]";
            }

            //20160512 zoearth 新增socket同步 contactRoom room名稱.changed事件
            var content = {
                "action" : "renew",
                "id"     : userData.guid,
            };
            sails.sockets.broadcast('contactRoom','changed',content);

            return res.redirect(sails.getUrlFor('contactController.list'));
        });
    },

    //刪除資料
    delete: function (req, res)
    {
        var viewData = z2.setViewData(req,res);
        viewData.title = "聯絡我們表單-刪除資料";

        //是否為編輯
        var post = req.allParams();
        userData = {};
        //取得資料
        if (!(typeof post.guid != 'undefined' && post.guid > 0 ))
        {
            //提示訊息
            return res.serverError('ERROR 0120 找不到使用者!');
        }

        db_contact.delUser(post.guid,function(us){
            userData = us;
            req.session.message = "刪除完成["+userData.name+"]";

            //20160512 zoearth 新增socket同步 contactRoom room名稱.changed事件
            var content = {
                "action" : "del",
                "id"     : post.guid,
            };
            sails.sockets.broadcast('contactRoom','changed',content);

            //設定view的值
            return res.redirect(sails.getUrlFor('contactController.list'));
        });
    },
};
