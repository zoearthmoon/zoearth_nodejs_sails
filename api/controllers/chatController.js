//聯絡我們功能
module.exports = {
    //######################### socket 功能S ########################
    socket: function(req, res) {
        if (!req.isSocket)
        {
            return res.badRequest();
        }
        //開啟socket
        //contactRoom 連線room

        //離開時(在)
        console.log(typeof sails.config.sockets);
        console.log(typeof sails.config.sockets.onAfterDisconnect);

        sails.config.sockets.onAfterDisconnect.chatRoom = function (session, socket){
            if (typeof session.user == 'undefined')
            {
                return false;
            }
            var content = {
                guid: 0,
                msg: '[系統]:'+session.user.name+'離開了'+'('+z2.nowTime()+')',
                name: "",
            };
            sails.sockets.broadcast('chatRoom','newMsg',content);
        };

        sails.sockets.join(req, 'chatRoom', function(err) {
            if (err)
            {
                return res.serverError(err);
            }
            return res.ok();
        });
    },

    //20160518 zoearth 取得新訊息
    socketGetMsg: function (req, res)
    {
        if (!req.isSocket)
        {
            return res.badRequest();
        }

        var post = req.allParams();
        console.log(post);

        //取得用戶資料
        var userData = z2.getUserData(req);
        if (!userData)
        {
            return res.send(false);
        }

        var roomMsg = '';
        var msgId   = 0;
        //進入訊息
        if (post.action == 'intoRoom')
        {
            roomMsg = '[系統訊息]:'+userData.name+' 進入聊天室('+z2.nowTime()+')';
        }
        else if (post.msg != "")
        {
            msgId   = post.guid;
            roomMsg = '['+userData.name+']:'+post.msg+'('+z2.nowTime()+')';
        }
        var content = {
            guid: msgId,
            msg:  roomMsg,
            name: userData.name,
        };
        //發送訊息
        sails.sockets.broadcast('chatRoom','newMsg',content);
        return res.send(true);
    },

    //######################### socket 功能E ########################

    //action名稱.類似zend 中的action
    chat: function (req, res)
    {
        var viewData = z2.setViewData(req,res);
        viewData.title = "聊天室"+(z2.isLogin(req) ? '(已登入)':'(未登入)');

        //20160517 zoearth 必須登入
        if (!z2.isLogin(req))
        {
            req.session.message = "請先登入!";
            return res.redirect(sails.getUrlFor('usersController.login'));
        }


        return res.view('chat/list',viewData);
    },
};
