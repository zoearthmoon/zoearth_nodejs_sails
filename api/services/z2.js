//通用設定

//時間參數
var date   = new Date();
var moment = require("moment");
//md5用
var md5    = require("blueimp-md5");

module.exports = {
    url:        'http://localhost:1337',
    date:       date,
    dateMonth:  date.getMonth(),
    dateYear:   date.getFullYear(),
    moment:     moment,
    nowTime:    function(dateFormat){
            if (!dateFormat)
            {
                dateFormat = 'YYYY-MM-DD h:mm:ss';
            }
            return z2.moment(z2.date).format(dateFormat);
        },
    setViewData:function(req, res){
            z2.req = req;
            z2.res = res;
            return {req:req,res:res};
        },
    res:        {},//共用res
    req:        {},//共用req
    md5:        md5,

    //20160516 zoearth 會員相關helper
    isLogin:    function(req){
            if (typeof req.session.user != "undefined" && req.session.user.guid > 0 )
            {
                return true;
            }
            return false;
        },
    //設定登入
    setLogin:  function(req,userData){
            req.session.user = userData;
            return true;
        },
    //設定登出
    setLogout:  function(req){
            if (typeof req.session.user != "undefined" )
            {
                delete req.session.user;
            }
            return true;
        },
    //取得會員資料
    getUserData:function(req){
            if (typeof req.session.user != "undefined")
            {
                return req.session.user;
            }
            return false;
        },
};
