//資料庫
module.exports = {
    attributes: {
        guid: {
            type: 'int',
            autoIncrement: true,
            primaryKey: true,
            size: 11,
        },
        username: {
            type: 'string'
        },
        password: {
            type: 'string'
        },
    },

    //20160516 zoearth 這邊直接使用聯絡我們資料做會員登入，不另外新增table了
    getUsers: function (cb) {
        db_contact.find({}).exec(function(err,users){
            return cb(users);
        });
    },

    //取得單一資料
    getUserById: function (guid,cb) {
        db_contact.findOne().where({guid: guid}).exec(function (err,user) {
            return cb(user);
        });
    },
    getUserByUsername: function (username,cb) {
        db_contact.findOne().where({username: username}).exec(function (err,user) {
            return cb(user);
        });
    },

    //檢查登入
    checkLogin: function (username,password) {
        //加密
        password = z2.md5(password);
        return db_contact.findOne().where({username: username,password: password});
    },

    //執行登入
    setLogin: function (req,username,password,cb) {
        //再次檢查帳號密碼
        db_users.checkLogin(username,password).then(function(checkOk){
            //成功則取得會員資料.寫入session
            if (checkOk)
            {
                db_users.getUserByUsername(username,function (userData) {
                    //登入動作
                    z2.setLogin(req,userData);
                    return cb(true);
                });
            }
            else
            {
                return cb(false);
            }
        });
    },

}
