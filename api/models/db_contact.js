//資料庫
module.exports = {
    attributes: {
        guid: {
            type: 'int',
            autoIncrement: true,
            primaryKey: true,
            size: 11,
        },
        username: {
            type:   'string',
            unique: true
        },
        password: {
            type: 'string'
        },
        name: {
            type: 'string'
        },
        gender: {
            type: 'int',
            size: 1,
        },
        birthday: {
            type: 'date',
        },
        tel: {
            type: 'string',
            size: 20,
        },
        address: {
            type: 'string',
            size: 50,
        },
        note: {
            type: 'string',
            size: 200,
        },
    },

    //設定欄位.避免錯誤
    getAttrs:function(ua,atts,type){
        //設定為空白
        if (typeof type != 'undefined' && type == '1' )
        {
            ua.guid = '';
            ua.name = '';
            ua.gender = '';
            ua.birthday = '';
            ua.tel = '';
            ua.address = '';
            ua.note = '';
        }

        //預設，有則設定，無則跳過
        if (typeof atts.guid != 'undefined' )
            ua.guid = atts.guid;

        if (ua.guid > 0 )
        {
            if (typeof atts.username != 'undefined' )
                ua.username = atts.username;
        }

        if (typeof atts.name != 'undefined' )
            ua.name = atts.name;
        if (typeof atts.gender != 'undefined' )
            ua.gender = atts.gender;
        if (typeof atts.birthday != 'undefined' )
            ua.birthday = atts.birthday;
        if (typeof atts.tel != 'undefined' )
            ua.tel = atts.tel;
        if (typeof atts.address != 'undefined' )
            ua.address = atts.address;
        if (typeof atts.note != 'undefined' )
            ua.note = atts.note;

        //20160516 zoearth 新增密碼
        if (typeof atts.password != 'undefined' && atts.password != '' )
        {
            ua.password = z2.md5(atts.password);
        }
        else
        {
            delete ua.password;
        }

        return ua;
    },

    //取得列表資料
    getUsers: function (cb) {
        db_contact.find({}).exec(function(err,users){
            return cb(users);
        });
    },

    //取得單一資料
    getUser: function (guid,cb) {
        db_contact.findOne().where({guid: guid})
            .exec(function (err,user) {
                return cb(user);
            });
    },

    //新增修改資料
    creatAndSave: function (userData,cb) {
        //設定欄位.避免錯誤
        db_contact.findOne().where({guid: (typeof userData.guid != 'undefined' ) ? userData.guid:0})
            .then(function (ua) {
                if (ua)
                {
                    ua = db_contact.getAttrs(ua,userData);
                    ua.save(function (err){
                        //callback 回傳 ua 會員資料
                        return cb(ua);
                    });
                }
                else
                {
                    ua = {};
                    ua = db_contact.getAttrs(ua,userData);
                    ua.username = userData.username;
                    db_contact.create(ua).exec(function (err,createData){
                        //callback 回傳 ua 會員資料
                        return cb(createData);
                    });
                }
            });
    },

    //刪除資料
    delUser: function (guid,cb) {
        db_contact.findOne().where({guid: guid})
            .then(function (ua) {
                if (ua)
                {
                    //設定欄位.避免錯誤
                    db_contact.destroy({guid: guid}).exec(function (err){
                        //callback 回傳 ua 會員資料
                        return cb(ua);
                    });
                }
            });
    },
}
